## Steps to run the project
Prequisite: Docker, Maven, Java 8
1. Open the terminal
2. Go to the root folder of project where pom.xml is present.
3. run the maven build with command (mvn clean install)
4. run the command docker-compose up

## cURL POST:
curl --location --request POST 'http://localhost:8080/travelapi/updatePrice' \
--header 'Content-Type: application/json' \
--data-raw '{
    "hotelId": 1,
    "roomId": 2,
    "prices": {
        "client-id": 2,
        "price": {
            "default": 200,
            "vip": 194.00,
            "discount": 198.25
        }
    }
}'

## cURL GET:
curl --location --request GET 'http://localhost:8080/travelapi/getRooms/3?roomId=300&client-id=2&loyalty=10&limit=5'
## Useful Commands
docker build -t agoda/hotelserviceapp .
docker run -p 8080:8080 agoda/hotelserviceapp

docker run --detach --env MYSQL_ROOT_PASSWORD=root --env MYSQL_USER=agodauser --env MYSQL_PASSWORD=agodapassword --env MYSQL_DATABASE=roomsDB --name mysql --publish 3306:3306 mysql:5.7
