FROM openjdk:8-jdk-alpine

RUN addgroup -S admin && adduser -S admin -G admin
USER admin:admin

VOLUME /tmp
COPY target/*.jar app.jar
ENV JAVA_OPTS=""

ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar" ]
