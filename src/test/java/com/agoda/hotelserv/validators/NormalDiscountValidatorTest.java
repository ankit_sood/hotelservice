package com.agoda.hotelserv.validators;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class NormalDiscountValidatorTest {
private NormalDiscountValidator normalDiscountValidator;
	
	@BeforeEach
	void initUseCase() {
		normalDiscountValidator = new NormalDiscountValidator();
	}

	@Test
	void validVIPDiscountTest() {
		Map<String, Double> priceMap = new HashMap<>();
		priceMap.put("default", 200D);
		priceMap.put("discount", 198.00D);
		boolean isValid = normalDiscountValidator.isValid(priceMap, null);
		assertEquals(true, isValid);
	}
	
	@Test
	void invalidVIPDiscountTest() {
		Map<String, Double> priceMap = new HashMap<>();
		priceMap.put("default", 200D);
		priceMap.put("discount", 104.00D);
		boolean isValid = normalDiscountValidator.isValid(priceMap, null);
		assertEquals(false, isValid);
	}
}
