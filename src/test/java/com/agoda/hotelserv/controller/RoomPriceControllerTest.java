package com.agoda.hotelserv.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import com.agoda.hotelserv.exceptions.RecordCreationException;
import com.agoda.hotelserv.models.Parameters;
import com.agoda.hotelserv.models.Response;
import com.agoda.hotelserv.models.RoomPrice;
import com.agoda.hotelserv.models.RoomPriceResponse;
import com.agoda.hotelserv.service.RoomPriceService;

@ExtendWith(MockitoExtension.class)
public class RoomPriceControllerTest {
	@Mock
	private RoomPriceService roomsService;
	
	@Mock
	private Parameters parameters;
	
	@Mock
	private BindingResult bindingResult;
	
	@Mock
	private RoomPrice roomPriceRequest;
	
	private RoomPriceController roomPriceController;
	
	
	@BeforeEach
	void initUseCase() {
		roomPriceController = new RoomPriceController(roomsService);
	}
	
	@Test
	void getAllRoomDetailsTest_Success() {
		Integer hotelId = 200; 
		Mockito.when(roomsService.getRooms(hotelId, parameters)).thenReturn(getRoomPriceResponse(hotelId));
		ResponseEntity<RoomPriceResponse> actualResponse = roomPriceController.getRoomDetails(hotelId, parameters);
		assertNotNull(actualResponse);
		assertEquals(HttpStatus.OK, actualResponse.getStatusCode());
	}
	
	@Test
	void getAllRoomDetailsTest_NotFound() {
		Integer hotelId = 200; 
		Mockito.when(roomsService.getRooms(hotelId, parameters)).thenReturn(null);
		ResponseEntity<RoomPriceResponse> actualResponse = roomPriceController.getRoomDetails(hotelId, parameters);
		assertNotNull(actualResponse);
		assertEquals(HttpStatus.NOT_FOUND, actualResponse.getStatusCode());
	}
	
	@Test
	void saveRoomPricesTest_Success() throws RecordCreationException {
		Mockito.when(roomsService.savePrices(roomPriceRequest)).thenReturn(true);
		ResponseEntity<?> actualResponse = roomPriceController.saveRoomPrices(roomPriceRequest, bindingResult);
		
		assertNotNull(actualResponse);
		assertEquals(HttpStatus.OK, actualResponse.getStatusCode());
		assertNotNull(actualResponse.getBody());
		Response responseBody = (Response) actualResponse.getBody();
		assertEquals("true", responseBody.getStatus());
	}
	
	@Test
	void saveRoomPricesTest_Failure() throws RecordCreationException {
		ObjectError objectError = new ObjectError("roomPriceRequest", "Invalid Discount");
		List<ObjectError> objectErrors = new ArrayList<>();
		objectErrors.add(objectError);
		
		Mockito.when(bindingResult.hasErrors()).thenReturn(true);
		Mockito.when(bindingResult.getAllErrors()).thenReturn(objectErrors);
		ResponseEntity<?> actualResponse = roomPriceController.saveRoomPrices(roomPriceRequest, bindingResult);
		
		assertNotNull(actualResponse);
		assertEquals(HttpStatus.BAD_REQUEST, actualResponse.getStatusCode());
		assertNotNull(actualResponse.getBody());
		Response responseBody = (Response) actualResponse.getBody();
		assertEquals("false", responseBody.getStatus());
	}
	
	@Test
	void saveRoomPricesTest_Ex() throws RecordCreationException {
		RecordCreationException exp = new RecordCreationException("Record Creation Issue.");
		Mockito.when(roomsService.savePrices(roomPriceRequest)).thenThrow(exp);
		Assertions.assertThrows(RecordCreationException.class, () -> {
			roomPriceController.saveRoomPrices(roomPriceRequest, bindingResult);
		});
	}
	
	private RoomPriceResponse getRoomPriceResponse(Integer hotelId) {
		RoomPriceResponse roomPriceResponse = new RoomPriceResponse();
		roomPriceResponse.setHotelId(hotelId);
		return roomPriceResponse;
	}
}
