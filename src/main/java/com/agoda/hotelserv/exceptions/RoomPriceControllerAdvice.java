package com.agoda.hotelserv.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.agoda.hotelserv.models.ErrorMessage;

@ControllerAdvice
public class RoomPriceControllerAdvice {
	
	@ExceptionHandler(RecordCreationException.class)
	ResponseEntity<ErrorMessage> handleRecordCreationException(RecordCreationException ex) {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(getErrorMessage(ex, "Error Occured while inserting into Database."));
	}
	
	@ExceptionHandler(RuntimeException.class)
	ResponseEntity<ErrorMessage> handleAllExceptions(RuntimeException ex) {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(getErrorMessage(ex, "Unexpected Error Occurred"));
	}
	
	private ErrorMessage getErrorMessage(Exception ex, String message) {
		ErrorMessage errorMessage = new ErrorMessage();
		errorMessage.setMessage(message);
		errorMessage.setSuccess(false);
		errorMessage.setCause(ex.getMessage());
		return errorMessage;
	}
}
