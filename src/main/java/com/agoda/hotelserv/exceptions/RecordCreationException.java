package com.agoda.hotelserv.exceptions;

public class RecordCreationException extends Exception {
	private static final long serialVersionUID = -800290535705058129L;

	public RecordCreationException(String errorMessage) {
		super(errorMessage);
	}

	public RecordCreationException(String message, Throwable cause) {
		super(message, cause);
	}

	public RecordCreationException(Throwable cause) {
		super(cause);
	}
}
