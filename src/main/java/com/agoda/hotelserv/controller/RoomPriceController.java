package com.agoda.hotelserv.controller;

import static com.agoda.hotelserv.constants.Constants.GET_ROOMS;
import static com.agoda.hotelserv.constants.Constants.HOTEL_ID;
import static com.agoda.hotelserv.constants.Constants.TRAVEL_API;
import static com.agoda.hotelserv.constants.Constants.UPDATE_PRICE;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.agoda.hotelserv.exceptions.RecordCreationException;
import com.agoda.hotelserv.models.Parameters;
import com.agoda.hotelserv.models.Response;
import com.agoda.hotelserv.models.RoomPrice;
import com.agoda.hotelserv.models.RoomPriceResponse;
import com.agoda.hotelserv.service.RoomPriceService;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(TRAVEL_API)
@AllArgsConstructor
public class RoomPriceController {

	private final RoomPriceService roomsService;
	
	@GetMapping(GET_ROOMS)
	public ResponseEntity<RoomPriceResponse> getRoomDetails(@PathVariable(HOTEL_ID)Integer hotelId, Parameters parameters) {
		RoomPriceResponse roomPriceResponse  = roomsService.getRooms(hotelId, parameters);
		if(roomPriceResponse != null) {
			return ResponseEntity.ok(roomsService.getRooms(hotelId, parameters));
		}
		log.info("No results found. hotelId: {}, Parameters: {}", hotelId, parameters);
		return ResponseEntity.notFound().build();
	}
	
	@PostMapping(UPDATE_PRICE)
	public ResponseEntity<?> saveRoomPrices(@Valid @RequestBody RoomPrice roomPriceRequest,
			BindingResult bindingResult) throws RecordCreationException {
		if(bindingResult.hasErrors()) {
			log.info("Request Received is Invalid. Request: {}", roomPriceRequest);
			List<String> errorList = bindingResult.getAllErrors().stream()
												  .map(ObjectError::getDefaultMessage)
												  .collect(Collectors.toList());
			Response insertResponse = new Response();
			insertResponse.setErrors(errorList);
			return ResponseEntity.badRequest().body(prepareInsertResponse(false, errorList));
		}
		log.info("Process to Insert/Update Room Rent Initiated.");
		boolean isSuccess = roomsService.savePrices(roomPriceRequest);
		return ResponseEntity.ok(prepareInsertResponse(isSuccess, null));
	}
	
	private Response prepareInsertResponse(boolean isSuccess, List<String> errorList) {
		Response insertResponse = new Response();
		insertResponse.setStatus(String.valueOf(isSuccess));
		insertResponse.setErrors(errorList);
		return insertResponse;
	}
	
}
