package com.agoda.hotelserv.tasks;

import java.util.concurrent.Callable;

import org.springframework.data.jpa.repository.JpaRepository;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public abstract class InsertionTask<T,K> implements Callable<Boolean>{
	protected JpaRepository<T,K> jpaRepository;
	protected T entity;
	
	public abstract Boolean call() throws Exception;
	
}
