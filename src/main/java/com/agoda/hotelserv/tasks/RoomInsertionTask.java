package com.agoda.hotelserv.tasks;

import org.springframework.data.jpa.repository.JpaRepository;

import com.agoda.hotelserv.entities.RoomEntity;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RoomInsertionTask extends InsertionTask<RoomEntity, Integer> {

	public RoomInsertionTask(JpaRepository<RoomEntity, Integer> jpaRepository, RoomEntity entity) {
		super(jpaRepository, entity);
	}

	@Override
	public Boolean call() throws Exception {
		jpaRepository.save(entity);
		log.info("Insertion triggered for {}", entity);
		return true;
	}
	
}
