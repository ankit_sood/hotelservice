package com.agoda.hotelserv.models;

import javax.validation.Valid;

import com.agoda.hotelserv.validators.RequiredPrice;

import lombok.Data;

@Data
public class RoomPrice {
	private Integer hotelId;
	
	private Integer roomId;
	
	@Valid
	@RequiredPrice
	private Price prices;
}
