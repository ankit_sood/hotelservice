package com.agoda.hotelserv.models;

import java.util.Map;

import com.agoda.hotelserv.validators.NormalDiscount;
import com.agoda.hotelserv.validators.VipDiscount;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Price {
	@JsonProperty("client-id")
	private Integer clientId;
	
	@VipDiscount
	@NormalDiscount
	@JsonProperty("price")
	private Map<String, Double> priceMap;
}
