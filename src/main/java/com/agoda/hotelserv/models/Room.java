package com.agoda.hotelserv.models;

import java.util.List;

import lombok.Data;

@Data
public class Room {
	private Integer roomId;
	private List<Price> prices;
}
