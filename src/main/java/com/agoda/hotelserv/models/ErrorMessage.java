package com.agoda.hotelserv.models;

import lombok.Data;

@Data
public class ErrorMessage {
	private String message;
	private boolean isSuccess;
	private String cause;
}
