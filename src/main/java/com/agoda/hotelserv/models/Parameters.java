package com.agoda.hotelserv.models;

import lombok.Data;

@Data
public class Parameters {
	private Integer roomId;
	private Integer clientId;
	private Integer loyalty;
	private Integer limit;
}
