package com.agoda.hotelserv.models;

import java.util.List;

import lombok.Data;

@Data
public class RoomPriceResponse {
	private Integer hotelId;
	private List<Room> rooms;
}
