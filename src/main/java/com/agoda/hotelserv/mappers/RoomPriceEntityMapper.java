package com.agoda.hotelserv.mappers;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.mapstruct.Mapper;

import com.agoda.hotelserv.constants.LoyaltyLevel;
import com.agoda.hotelserv.entities.RoomEntity;
import com.agoda.hotelserv.models.Price;
import com.agoda.hotelserv.models.Room;
import com.agoda.hotelserv.models.RoomPriceResponse;

@Mapper(componentModel = "spring")
public interface RoomPriceEntityMapper {

	default RoomPriceResponse getRoomPriceFromEntity(List<RoomEntity> roomEntity) {
		RoomPriceResponse roomPriceResponse = new RoomPriceResponse();

		Map<Integer, List<RoomEntity>> roomsMap = roomEntity.stream()
				.collect(Collectors.groupingBy(RoomEntity::getRoomId));

		List<Room> rooms = roomsMap.entrySet().stream().map(e -> {
			Room room = new Room();
			room.setRoomId(e.getKey());
			Map<Integer, List<RoomEntity>> clientsMap = roomsMap.get(e.getKey()).stream()
					.collect(Collectors.groupingBy(RoomEntity::getClientId));
			List<Price> prices = clientsMap.entrySet().stream().map(entry -> {
				Price price = new Price();
				price.setClientId(entry.getKey());
				Map<String, Double> priceMap = clientsMap.get(entry.getKey()).stream().collect(
						Collectors.toMap(r -> LoyaltyLevel.getLoyaltyLevel(r.getLoyaltyLevel()), RoomEntity::getPrice));
				price.setPriceMap(priceMap);
				return price;
			}).collect(Collectors.toList());

			room.setPrices(prices);
			return room;
		}).collect(Collectors.toList());

		roomPriceResponse.setHotelId(roomEntity.get(0).getHotelId());
		roomPriceResponse.setRooms(rooms);
		return roomPriceResponse;
	}

}
