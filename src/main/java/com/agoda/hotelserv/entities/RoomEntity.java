package com.agoda.hotelserv.entities;

import static com.agoda.hotelserv.constants.EntityConstants.CLIENT_ID;
import static com.agoda.hotelserv.constants.EntityConstants.HOTEL_ID;
import static com.agoda.hotelserv.constants.EntityConstants.ID;
import static com.agoda.hotelserv.constants.EntityConstants.LOYALTY_LEVEL;
import static com.agoda.hotelserv.constants.EntityConstants.PRICE;
import static com.agoda.hotelserv.constants.EntityConstants.ROOM_ID;
import static com.agoda.hotelserv.constants.EntityConstants.CREATE_DATE;
import static com.agoda.hotelserv.constants.EntityConstants.UPDATE_DATE;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@Table(name = "PRICES")
@NoArgsConstructor
@AllArgsConstructor
public class RoomEntity implements Serializable{
	private static final long serialVersionUID = -8461970762549139303L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name=ID)
	private Integer id;
	
	@Column(name=HOTEL_ID)
	private Integer hotelId;
	
	@Column(name=ROOM_ID)
	private Integer roomId;
	
	@Column(name=CLIENT_ID)
	private Integer clientId;
	
	@Column(name=LOYALTY_LEVEL)
	private Integer loyaltyLevel;
	
	@Column(name=PRICE)
	private Double price;
	
	@CreationTimestamp
    @Column(name=CREATE_DATE,updatable = false)
    private Timestamp dateCreated;
	
    @UpdateTimestamp
    @Column(name=UPDATE_DATE)
    private Timestamp lastModified;
}
