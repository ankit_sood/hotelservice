package com.agoda.hotelserv.service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.agoda.hotelserv.constants.LoyaltyLevel;
import com.agoda.hotelserv.entities.RoomEntity;
import com.agoda.hotelserv.exceptions.RecordCreationException;
import com.agoda.hotelserv.mappers.RoomPriceEntityMapper;
import com.agoda.hotelserv.models.Parameters;
import com.agoda.hotelserv.models.RoomPrice;
import com.agoda.hotelserv.models.RoomPriceResponse;
import com.agoda.hotelserv.repository.PricesRepository;
import com.agoda.hotelserv.tasks.InsertionTask;
import com.agoda.hotelserv.tasks.RoomInsertionTask;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@AllArgsConstructor
public class RoomPriceService {
	private final PricesRepository pricesRepository; 
	private final ExecutorService executorService;
	private final RoomPriceEntityMapper roomPriceEntityMapper;
	
	@Transactional(rollbackOn = Exception.class, value = TxType.REQUIRES_NEW)
	public boolean savePrices(RoomPrice roomPriceRequest) throws RecordCreationException {
		Map<Integer, RoomEntity> existingRoomPrices = getExistingRoomPrices(roomPriceRequest);
		Map<String, Double> priceMap = roomPriceRequest.getPrices().getPriceMap();
		List<Future<Boolean>> roomPrices = priceMap.keySet().parallelStream()
									.map(loyalty -> executorService.submit(getInsertionTask(existingRoomPrices, roomPriceRequest, loyalty, priceMap)))
									.collect(Collectors.toList());
		log.info("Insertion tasks submitted successfully.");
		return verifyInsertion(roomPrices);
	}
	
	public RoomPriceResponse getRooms(Integer hotelId, Parameters parameters){
		int limit = parameters.getLimit() != null ? parameters.getLimit() : 2;
		List<RoomEntity> rooms = null;
		if(parameters.getRoomId()==null && parameters.getClientId()==null && 
				parameters.getLoyalty()==null) {
			rooms = pricesRepository.findRoomsByHotelId(hotelId, PageRequest.of(0, limit));
		} else {
			rooms = pricesRepository.findRooms(hotelId, parameters.getRoomId(), parameters.getClientId(), 
					parameters.getLoyalty(), PageRequest.of(0, limit));
		}
		return !CollectionUtils.isEmpty(rooms) ? roomPriceEntityMapper.getRoomPriceFromEntity(rooms) : null;
	}
	
	private InsertionTask<RoomEntity, Integer> getInsertionTask(Map<Integer, RoomEntity> existingRoomPrices, RoomPrice roomPriceRequest, 
			String loyalty, Map<String, Double> priceMap) {
		RoomEntity roomEntity = existingRoomPrices.get(LoyaltyLevel.getLoyaltyCode(loyalty));
		if(roomEntity == null) {
			roomEntity = prepareRoomEntity(roomPriceRequest, loyalty, priceMap.get(loyalty));
			log.info("Created Entity for client: {}, loyalyLevel: {}",
					roomEntity.getClientId(), roomEntity.getLoyaltyLevel());
		} else {
			roomEntity.setPrice(priceMap.get(loyalty));
			log.info("Updated Entity for client: {}, loyalyLevel: {}",
					roomEntity.getClientId(), roomEntity.getLoyaltyLevel());
		}
		return new RoomInsertionTask(pricesRepository, roomEntity);
	}
	
	private Map<Integer, RoomEntity> getExistingRoomPrices(RoomPrice roomPriceRequest){
		List<RoomEntity> rooms = pricesRepository.findByHotelIdAndRoomIdAndClientId(roomPriceRequest.getHotelId(), 
				roomPriceRequest.getRoomId(), roomPriceRequest.getPrices().getClientId());
		
		return rooms.stream().collect(Collectors.toMap(RoomEntity::getLoyaltyLevel, room -> room));
	}
	
	private boolean verifyInsertion(List<Future<Boolean>> roomPrices) throws RecordCreationException {
		boolean isSuccess = true;
		for(Future<Boolean> future: roomPrices) {
			try {
				isSuccess = isSuccess && future.get();
			} catch (InterruptedException | ExecutionException exp) {
				log.error("Exception occurred while inserting the record. Message: {} ", exp.getMessage());
				throw new RecordCreationException(exp);
			}
		}
		log.info("All Insertion/Updation performed successfully.");
		return isSuccess;
	}

	private RoomEntity prepareRoomEntity(RoomPrice roomPriceRequest, String loyalty, Double price) {
		RoomEntity room = new RoomEntity();
		room.setHotelId(roomPriceRequest.getHotelId());
		room.setRoomId(roomPriceRequest.getRoomId());
		room.setLoyaltyLevel(LoyaltyLevel.getLoyaltyCode(loyalty));
		room.setClientId(roomPriceRequest.getPrices().getClientId());
		room.setPrice(price);
		return room;
	}
}
