package com.agoda.hotelserv.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.agoda.hotelserv.entities.RoomEntity;

@Repository
public interface PricesRepository extends JpaRepository<RoomEntity, Integer>{
	
	@Query("SELECT r FROM RoomEntity r WHERE (:hotelId is null or r.hotelId = :hotelId) AND "
			+ "(:roomId is null or r.roomId = :roomId) AND (:clientId is null or r.clientId = :clientId) "
			+ "AND (:loyaltyLevel is null or r.loyaltyLevel = :loyaltyLevel)")
	List<RoomEntity> findRooms(Integer hotelId, Integer roomId, Integer clientId, Integer loyaltyLevel, Pageable pageable);
	
	List<RoomEntity> findByHotelIdAndRoomIdAndClientId(Integer hotelId, Integer roomId, Integer clientId);
	
	@Query("SELECT r FROM RoomEntity r WHERE r.roomId IN (SELECT r1.roomId FROM RoomEntity r1 "
			+ "WHERE r.hotelId = :hotelId GROUP BY r1.roomId)")
	List<RoomEntity> findRoomsByHotelId(Integer hotelId, Pageable pageable);

}
