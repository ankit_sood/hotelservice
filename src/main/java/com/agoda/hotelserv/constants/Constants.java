package com.agoda.hotelserv.constants;

public class Constants {
	public static final String EXECUTOR_SERVICE = "executorService";
	public static final String TRAVEL_API = "/travelapi";
	public static final String GET_ROOMS = "/getRooms/{hotelId}";
	public static final String UPDATE_PRICE = "/updatePrice";
	public static final String HOTEL_ID = "hotelId";
	public static final String CORRELATION_ID_HEADER = "X-Correlation-ID";
}
