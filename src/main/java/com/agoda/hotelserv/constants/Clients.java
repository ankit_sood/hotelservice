package com.agoda.hotelserv.constants;

public enum Clients {
	DESKTOP(1), MOBILE_APP(2), GOOGLE_SEARCH(2);
	
	private Integer clientId;
	
	Clients(Integer clientId) {
		this.clientId = clientId;
	}

	public Integer getClientId() {
		return this.clientId;
	}
}
