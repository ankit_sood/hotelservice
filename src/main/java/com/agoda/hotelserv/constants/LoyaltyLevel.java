package com.agoda.hotelserv.constants;

public enum LoyaltyLevel {
	DEFAULT(10, "default"), DISCOUNT(11, "discount"), VIP(12, "vip");
	
	private Integer loyaltyCode;
	private String loyaltyLevel;
	
	LoyaltyLevel(Integer loyaltyCode, String loyaltyLevel) {
		this.loyaltyCode = loyaltyCode;
		this.loyaltyLevel = loyaltyLevel;
	}

	public Integer getLoyaltyCode() {
		return this.loyaltyCode;
	}
	
	public String getLoyaltyLevel() {
		return this.loyaltyLevel;
	}
	
	public static Integer getLoyaltyCode(String loyaltyLevelVal) {
		if(DEFAULT.toString().equalsIgnoreCase(loyaltyLevelVal)) {
			return DEFAULT.loyaltyCode;
		}else if(DISCOUNT.toString().equalsIgnoreCase(loyaltyLevelVal)) {
			return DISCOUNT.loyaltyCode;
		}else if(VIP.toString().equalsIgnoreCase(loyaltyLevelVal)) {
			return VIP.loyaltyCode;
		}
		return -1;
	}
	
	public static String getLoyaltyLevel(Integer loyaltyCode) {
		if(DEFAULT.getLoyaltyCode().equals(loyaltyCode)) {
			return DEFAULT.getLoyaltyLevel();
		}else if(DISCOUNT.getLoyaltyCode().equals(loyaltyCode)) {
			return DISCOUNT.getLoyaltyLevel();
		}else if(VIP.getLoyaltyCode().equals(loyaltyCode)) {
			return VIP.getLoyaltyLevel();
		}
		return "";
	}
}
