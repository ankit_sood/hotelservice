package com.agoda.hotelserv.constants;

public class EntityConstants {
	public static final String ID = "ID";
	public static final String HOTEL_ID = "HOTEL_ID";
	public static final String ROOM_ID = "ROOM_ID";
	public static final String CLIENT_ID = "CLIENT_ID";
	public static final String LOYALTY_LEVEL = "LOYALTY_LEVEL";
	public static final String PRICE = "PRICE";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String UPDATE_DATE = "UPDATE_DATE";
}
