package com.agoda.hotelserv.validators;

import java.util.Map;

import com.agoda.hotelserv.constants.LoyaltyLevel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class DiscountValidator {
	public boolean validateDiscount(Map<String, Double> priceMap, Double percentage, String loyaltyLevel) {
		Double defaultPrice = priceMap.getOrDefault(LoyaltyLevel.DEFAULT.getLoyaltyLevel(), 0D);
		Double discountPrice = defaultPrice - (defaultPrice * percentage);
		if(discountPrice <= priceMap.get(loyaltyLevel)) {
			return true;
		} else {
			log.info("Discounted Price value is not under permissable limits. LoyaltyLevel: {}, "
					+ "default: {}, Min Possible Price: {}", loyaltyLevel, defaultPrice, discountPrice);
			return false;
		}
	}
}	
