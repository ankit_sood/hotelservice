package com.agoda.hotelserv.validators;

import java.util.Map;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.agoda.hotelserv.constants.LoyaltyLevel;
import com.agoda.hotelserv.models.Price;

public class RequiredPriceValidator implements ConstraintValidator<RequiredPrice, Price> {

	@Override
	public boolean isValid(Price price, ConstraintValidatorContext context) {
		Map<String, Double> map = price.getPriceMap();
		if (map != null && map.containsKey(LoyaltyLevel.DEFAULT.getLoyaltyLevel())) {
			return true;
		}
		return false;
	}
}
