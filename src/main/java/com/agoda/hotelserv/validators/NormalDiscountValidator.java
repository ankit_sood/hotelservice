package com.agoda.hotelserv.validators;

import java.util.Map;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.agoda.hotelserv.constants.LoyaltyLevel;

public class NormalDiscountValidator extends DiscountValidator
		implements ConstraintValidator<NormalDiscount, Map<String, Double>> {
	@Override
	public boolean isValid(Map<String, Double> priceMap, ConstraintValidatorContext context) {
		boolean isValid = true;
		if (priceMap.containsKey(LoyaltyLevel.DISCOUNT.getLoyaltyLevel())) {
			isValid = validateDiscount(priceMap, 0.05, LoyaltyLevel.DISCOUNT.getLoyaltyLevel());
		}
		return isValid;
	}
}
