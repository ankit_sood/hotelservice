package com.agoda.hotelserv.validators;

import java.util.Map;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.agoda.hotelserv.constants.LoyaltyLevel;

public class VipDiscountValidator extends DiscountValidator
		implements ConstraintValidator<VipDiscount, Map<String, Double>> {

	@Override
	public boolean isValid(Map<String, Double> priceMap, ConstraintValidatorContext context) {
		boolean isValid = true;
		if (isValid && priceMap.containsKey(LoyaltyLevel.VIP.getLoyaltyLevel())) {
			isValid = validateDiscount(priceMap, 0.1, LoyaltyLevel.VIP.getLoyaltyLevel());
		}
		return isValid;
	}
}
